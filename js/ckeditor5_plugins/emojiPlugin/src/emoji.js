/**
 * @file This is what CKEditor refers to as a master (glue) plugin. Its role is
 * just to load the “editing” and “UI” components of this Plugin. Those
 * components could be included in this file, but
 *
 * I.e, this file's purpose is to integrate all the separate parts of the plugin
 * before it's made discoverable via index.js.
 */
// cSpell:ignore Emoji Emoji

// The contents of Emoji and Emoji editing could be included in this
// file, but it is recommended to separate these concerns in different files.

// CkEditor
import { Plugin } from 'ckeditor5/src/core';
import { Typing } from 'ckeditor5/src/typing';
import { createDropdown } from 'ckeditor5/src/ui';
import { CKEditorError } from 'ckeditor5/src/utils';

// UI
import EmojiCharactersNavigationView from './ui/emojicharactersnavigationview';
import CharacterGridView from './ui/charactergridview';
import CharacterInfoView from './ui/characterinfoview';
import CharacterSearchView from './ui/charactersearchview';

// Emoji
import EmojiActivity from './emoji-activity';
import EmojiFlags from './emoji-flags';
import EmojiFood from './emoji-food';
import EmojiNature from './emoji-nature';
import EmojiObjects from './emoji-objects';
import EmojiPeople from './emoji-people';
import EmojiPlaces from './emoji-places';
import EmojiSymbols from './emoji-symbols';


import emojIIcon from '../theme/icons/emoji-icon.svg';
const ALL_EMOJI_CHARACTERS_GROUP = 'All';

export default class Emoji extends Plugin {

  /**
 * @inheritDoc
 */
  static get requires() {
    return [Typing];
  }

  /**
    * @inheritDoc
  */
  static get pluginName() {
    return 'Emoji';
  }

  /**
   * @inheritDoc
   */
  constructor(editor) {
    super(editor);
    this._characters = new Map();
    this._groups = new Map();

    const emojis = this.getEmojis();

    emojis.forEach(emoji => {
      this.addItems(emoji.prototype.getEmoji()[0], emoji.prototype.getEmoji()[1]);
    });
  }

  /**
   * @inheritDoc
   */
  init() {
    const editor = this.editor;
    const t = editor.t;

    const inputCommand = editor.commands.get('input');

    // This will register the Emoji toolbar button.
    editor.ui.componentFactory.add('Emoji', (locale) => {
      const inputCommand = editor.commands.get('input');
      const dropdownView = new createDropdown(locale);
        let dropdownPanelContent;


      // Create the toolbar button.
      dropdownView.buttonView.set({
        label: editor.t('Emoji'),
        icon: emojIIcon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
        dropdownView.bind('isEnabled').to(inputCommand);

      // Execute the command when the button is clicked (executed).

      // Insert a special character when a tile was clicked.
      dropdownView.on('execute', (evt, data) => {
        editor.execute('input', { text: data.character });
        editor.editing.view.focus();
      });

      dropdownView.on('change:isOpen', () => {
          if (!dropdownPanelContent) {
            dropdownPanelContent = this._createDropdownPanelContent(locale, dropdownView);

            dropdownView.panelView.children.add(dropdownPanelContent.navigationView);
            dropdownView.panelView.children.add(dropdownPanelContent.searchView);
            dropdownView.panelView.children.add(dropdownPanelContent.gridView);
            dropdownView.panelView.children.add(dropdownPanelContent.infoView);
          }

          dropdownPanelContent.infoView.set({
            character: null,
            name: null
          });
        });

        return dropdownView;
    });
  }

  getEmojis() {
    return [
      EmojiPeople,
      EmojiFood,
      EmojiNature,
      EmojiActivity,
      EmojiSymbols,
      EmojiPlaces,
      EmojiObjects,
      EmojiFlags
    ]
  }

  addItems(groupName, items) {
    if (groupName === ALL_EMOJI_CHARACTERS_GROUP) {
      throw new CKEditorError(
        `emoji-group-error-name: The name "${ALL_EMOJI_CHARACTERS_GROUP}" is reserved and cannot be used.`
      );
    }

    const group = this._getGroup(groupName);
    for (const item of items) {
      group.add(item.title);
      this._characters.set(item.title, item.character);
    }
  }

  getGroups() {
    return this._groups.keys();
  }

  getCharactersForGroup(groupName, characterContainString="") {

    if (groupName === ALL_EMOJI_CHARACTERS_GROUP) {
    let characters = new Set(this._characters.keys());
      if (characterContainString) {
        characters.forEach((character, index) => {
          if (!this.getCharacterContainedSearchedString(character, characterContainString)) {
            characters.delete(character);
          }
        });
      }
      return characters;
    }
    return this._groups.get(groupName);
  }

  getCharacterContainedSearchedString(character, searchString) {
    const lowerMainString = character.toLowerCase();
    const lowerSubString = searchString.toLowerCase();
    return lowerMainString.includes(lowerSubString);
  }

  getCharacter(title) {
    return this._characters.get(title);
  }

  _getGroup(groupName) {
    if (!this._groups.has(groupName)) {
      this._groups.set(groupName, new Set());
    }
    return this._groups.get(groupName);
  }

  _updateGrid(currentGroupName, gridView,characterContainString = '') {
    gridView.tiles.clear();
    if (characterContainString) {
      currentGroupName = 'All';
    }
    const characterTitles = this.getCharactersForGroup(currentGroupName,characterContainString);
    for (const title of characterTitles) {
      const character = this.getCharacter(title);
      gridView.tiles.add(gridView.createTile(character, title));
    }
  }

  debounce(callback, wait) {
    let timeout;
    return (...args) => {
      clearTimeout(timeout);
      timeout = setTimeout(function () { callback.apply(this, args); }, wait);
    };
  }

  _createDropdownPanelContent(locale, dropdownView) {
    const specialCharsGroups = [...this.getGroups()];

    // Add a special group that shows all available special characters.
    specialCharsGroups.unshift(ALL_EMOJI_CHARACTERS_GROUP);

    const navigationView = new EmojiCharactersNavigationView(locale, specialCharsGroups);
    const gridView = new CharacterGridView(locale);
    const searchView = new CharacterSearchView(locale,specialCharsGroups);
    const infoView = new CharacterInfoView(locale);

    gridView.delegate('execute').to(dropdownView);
    gridView.delegate('execute').to(searchView);

    gridView.on('tileHover', (evt, data) => {
      infoView.set(data);
    });

    // return search results.
    searchView.on('keyup', this.debounce((searchObj) => {
      if (searchObj.source.element.value) {
        this._updateGrid('All', gridView, searchObj.source.element.value);
      }else {
        this._updateGrid('All', gridView);
      }
    }, 500));

    // Update the grid of special characters when a user changed the character group.
    navigationView.on('execute', () => {
      this._updateGrid(navigationView.currentGroupName, gridView);
    });

    // Set the initial content of the special characters grid.
    this._updateGrid(navigationView.currentGroupName, gridView);

    return { searchView, navigationView, gridView, infoView };
  }

}
