/**
 * @license Copyright (c) 2003-2023, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @module ui/inputtext/inputtextview
 */

import {InputView} from 'ckeditor5/src/ui';

/**
 * The text input view class.
 */
export default class CharacterSearchView extends InputView {
  /**
   * @inheritDoc
   */
  constructor(locale) {
    super(locale);

    this.extendTemplate({
      attributes: {
        type: 'text',
        class: [
          'ck-input-text ck-emoji--search'
        ],
        placeholder : 'Search for emoji, e.g heart',
        style: 'margin: 0.850rem;',
        value: '',
      },
      on: {
        input: this.bindTemplate.to('keyup')
      }
    });
  }
}
