# CKEditor(5) Emoji Plugin for Drupal

## Introduction

The CKEditor Emoji Plugin is a module designed to enhance the content editing experience in Drupal by enabling the insertion of emojis directly into your text. Emojis have become a universal form of expression and communication, adding a fun and engaging aspect to your content. This module seamlessly integrates with CKEditor 5, providing an intuitive and user-friendly way to incorporate emojis in your Drupal content.

## Installation

To install the CKEditor Emoji Plugin, follow these steps:

1. Go to the Drupal admin dashboard.
2. Navigate to Configuration > Content authoring > Text formats and editors.
3. Edit the text format you want to enable emojis for (e.g., Full HTML).
4. In the CKEditor settings, add the Emoji icon button to the toolbar.
5. Save your changes.
6. Now, when you edit content using the configured text format, you'll have an emoji button in the CKEditor toolbar. Click it to insert emojis into your content seamlessly.

## Additional Requirements

The CKEditor Emoji Plugin for Drupal requires the following:

- CKEditor 5

## Similar Projects

While the CKEditor Emoji Plugin offers seamless integration for CKEditor in Drupal, you may also consider the "CKEditor Emoji" module designed for [CKEditor version 4](https://www.example.com/project/emoji). This alternative module provides similar functionality for users who are using CKEditor version 4. Depending on your specific CKEditor version and requirements, you can choose between the Drupal CKEditor Emoji Plugin and the "CKEditor Emoji" module to enhance your content with emojis.

## Supporting this Module

If you find this module useful and wish to support its development, you can check for ways to contribute or offer financial support on the project's page.
